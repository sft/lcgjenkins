#!/bin/bash
# The following variables are expected:
#  SLOTNAME:  Nightly slot name
#  LABEL:     Worker node label (e.g. slc6)
#  COMPILER:  Compiler and version
#  BUILDTYPE: Build type (e.g. Release)
#  WORKSPACE: Directory where the job is running in the local machine
isdone=$1

this=$(dirname $0)
platform=`$this/getPlatform.py`

if [ $isdone == 1 ]; then
  touch $WORKSPACE/build/isDone-$platform
  echo "Created file $WORKSPACE/build/isDone-$platform"
elif [ $isdone == 2 ]; then
  touch $WORKSPACE/build/isDone-unstable-$platform
  echo "Created unstable file $WORKSPACE/build/isDone-unstable-$platform"
else
  echo "Removing local isDone files"
  rm -vf $WORKSPACE/build/isDone-$platform
  rm -vf $WORKSPACE/build/isDone-unstable-$platform
fi
