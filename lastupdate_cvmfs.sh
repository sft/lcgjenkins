#!/bin/bash -x

MODE=$1
echo $MODE

SUFX=""

LASTUPDATE_PATH=lastUpdate
LEASE=""
if [ "$MODE" = "nightly" ]; then
    echo "Nightly cvmfs."
    SUFX="-nightlies"
    LASTUPDATE_PATH=.lastUpdateDir/lastUpdate
    LEASE="/lcg/${LASTUPDATE_PATH}"
fi

input1=`date +"%Y-%m-%d %H:%M:%S"` 
input2=`hostname -f`
input3=`date +%s`

inputtotal="$input1 | $input2 | $input3" 
echo $inputtotal > $WORKSPACE/lastUpdate

sudo -i -u cvsft${SUFX}<<EOF
for iterations in {1..10}
do
  cvmfs_server transaction sft${SUFX}.cern.ch${LEASE}
  if [ "\$?" == "1" ]; then
    if  [[ "$iterations" == "10" ]]; then
      echo "Too many tries... exiting"
      exit 1
    else
       echo "Transaction is already open. Going to sleep..."
       sleep 10m
    fi
  else
    break
  fi
done

cp $WORKSPACE/lastUpdate /cvmfs/sft${SUFX}.cern.ch/lcg/${LASTUPDATE_PATH}
cd $HOME
cvmfs_server publish sft${SUFX}.cern.ch${LEASE}

EOF
