#!/bin/bash -x                                                                                                                                                                                                                           
if [ $# -ge 2 ]; then
  PLATFORM=$1; shift
  BUILDMODE=$1; shift

else
  echo "$0: expecting 2 arguments: [platform] [buildmode]"
  return
fi

weekday=`date +%a`
export PLATFORM
export WORKDIR=$WORKSPACE
export weekday
export BUILDMODE

array=(${PLATFORM//-/ })
comp=`echo ${array[2]}`

# Starting the cvmfs commands to ensure a transaction
sudo -i -u cvsft <<EOF
cvmfs_server transaction sft.cern.ch

if [ "\$?" == "1" ]; then
  echo "Transaction is already open. Exiting ..."
  exit 1
fi


echo "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
whoami
echo "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"

echo "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
echo $SLOT
echo LCG_${SLOT}_$PLATFORM.txt
echo "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"

abort=0 

echo "This is the value of the BUILDMODE: ---> ${BUILDMODE}"

if [ "${BUILDMODE}" == "nightlies" ]; then
   $WORKSPACE/lcgjenkins/clean_cvmfs_nightlies.py ${SLOT} $PLATFORM $weekday
   $WORKSPACE/lcgjenkins/lcginstall.py -y -u https://lcgpackages.web.cern.ch/tarFiles/nightlies/$SLOT/$weekday -r ${SLOT} -d LCG_${SLOT}_$PLATFORM.txt -p /cvmfs/sft.cern.ch/lcg/nightlies/${SLOT}/$weekday/ -e cvmfs
   if [ "\$?" == "0" ]; then                                                                                                      
     echo "Installation script has worked, we go on"
     abort=1
   else
     echo "there is an error installing the packages. Exiting ..."
     cd $HOME
     cvmfs_server abort -f sft.cern.ch
     exit 1
   fi

   cd  /cvmfs/sft.cern.ch/lcg/nightlies/${SLOT}/$weekday/
   abort=1
   test "\$abort" == "1" && wget https://lcgpackages.web.cern.ch/tarFiles/nightlies/${SLOT}/$weekday/isDone-$PLATFORM
   if [ "\$?" == "0" ]; then
      echo "The installation of the nightly is completed with all packages"
      abort=1 
   fi                                                                                             

   wget https://lcgpackages.web.cern.ch/tarFiles/nightlies/${SLOT}/$weekday/isDone-unstable-$PLATFORM
   if [ "\$?" == "0" ]; then
      echo "The installation of the nightly is not completed with all packages. We publish what we have and we finish here"
      cd $HOME
      cvmfs_server publish sft.cern.ch
      exit 0
   fi

   $WORKSPACE/lcgjenkins/splitglobalsummary.py ${SLOT} $PLATFORM $comp nightly 
   test "\$abort" == "1" && $WORKSPACE/lcgcmake/cmake/scripts/create_lcg_view.py -l /cvmfs/sft.cern.ch/lcg/nightlies/${SLOT}/$weekday -p $PLATFORM -d -B /cvmfs/sft.cern.ch/lcg/views/${SLOT}/$weekday/$PLATFORM 
   if [ "\$?" == "0" ]; then
      echo "The creation of the views has worked"
      abort=1
   else
      echo "The creation of the views has not worked. We exit here"
      cd $HOME
      cvmfs_server abort -f sft.cern.ch
      exit 1
   fi

   test "\$abort" == "1" && rm /cvmfs/sft.cern.ch/lcg/views/${SLOT}/latest/$PLATFORM
   cd /cvmfs/sft.cern.ch/lcg/views/${SLOT}/latest 
   ln -s ../$weekday/$PLATFORM
   cd $HOME 
   test "\$abort" == "1" && cvmfs_server publish  sft.cern.ch

else
   $WORKSPACE/lcgjenkins/lcginstall.py -u https://lcgpackages.web.cern.ch/tarFiles/releases -r ${SLOT} -d LCG_${SLOT}_$PLATFORM.txt -p /cvmfs/sft.cern.ch/lcg/releases -e cvmfs
   if [ "\$?" == "0" ]; then
      echo "Installation script has worked, we go on"
      abort=1
   else
      echo "There was a problem installing the packages, we exit here"
      cd $HOME
      cvmfs_server abort -f sft.cern.ch
      exit 1
   fi
   test "\$abort" == "1" && $WORKSPACE/lcgcmake/cmake/scripts/create_lcg_view.py -l /cvmfs/sft.cern.ch/lcg/releases/LCG_${SLOT} -p $PLATFORM -d -B /cvmfs/sft.cern.ch/lcg/views/${SLOT}/$PLATFORM
   cd $HOME
   cvmfs_server publish sft.cern.ch    
fi
EOF
