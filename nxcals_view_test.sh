#!/usr/bin/env bash
set -e # 'e' quits the script on error, 'x' prints every command for debugging 

#####################################################################################
#  This script tests if the features of a NXCALS view are working as expected       #
#  but the tests are only scratching on the surface to detect major errors.         #
#                                                                                   #
#  Created 2019-01-08 by Johannes Heinz (technical student) on request by SWAN      #
#####################################################################################

source ${TARGET_VIEW}/nxcals/nxcals_versions.txt
echo "------------------------------------------------------------------------------------------"
echo "NXCALS Java version:   ${NXCALS_JAVA_VERSION}"
echo "NXCALS Python version: ${NXCALS_PYTHON_VERSION}"
echo "------------------------------------------------------------------------------------------"
echo "Test server hostname:  $( hostname )"
echo

source ${TARGET_VIEW}/setup.sh

# Python smoke tests
# ------------------
echo "[PYTHON 1/3] Testing pip ..."
if pip list 2>/dev/null | grep nxcals
then echo "--> SUCCESS"; else echo "--> FAIL"; exit 1; fi
echo

echo "[PYTHON 2/3] Testing cern/nxcals ..."
if python -c "from cern import nxcals; print(nxcals)"
then echo "--> SUCCESS"; else echo "--> FAIL"; exit 1; fi
echo

echo "[PYTHON 3/3] Testing tests/cern ..."
if python -c "from tests import cern; print(cern)"
then echo "--> SUCCESS"; else echo "--> FAIL"; exit 1; fi
echo

# Java smoke tests
# ----------------
echo "[JAVA 1/1] Testing environment \$SPARK_DIST_CLASSPATH ..."
if echo $SPARK_DIST_CLASSPATH | grep nxcals
then echo "--> SUCCESS"; else echo "--> FAIL"; exit 1; fi
echo

# Further smoke tests using spark are very hard to achieve due to restricted access on the NXCALS side 
# and lots of dependencies and configuration that needs to be set up for a simple test
