#!/bin/bash -x

# This script is intended to be executed in the Jenkins MASTER ONLY

export EOS_MGM_URL="root://eosproject-l.cern.ch"
pwd
weekday=`date +%a`
txtfile=LCG_${LCG_VERSION}_${PLATFORM}.txt
isDone=isDone-${PLATFORM}
isDoneUnstable=isDone-unstable-${PLATFORM}
tarfiles=*-${PLATFORM}.tgz
# temporary solution for mixt of platforms - simple and with processor instruction sets
SHORTPLATFORM=${PLATFORM%%+*}-${PLATFORM#*-}

if [[ ${PLATFORM} == *slc6* || ${PLATFORM} == *cc7* || ${PLATFORM} == *centos7* || ${PLATFORM} == *ubuntu* || ${PLATFORM} == *fedora*  ]]; then
    kinit sftnight@CERN.CH -5 -V -k -t /ec/conf/sftnight.keytab
fi

if [ "${BUILDMODE}" == "nightly" ]; then
    basespace=/eos/project/l/lcg/www/lcgpackages/tarFiles/nightlies/${LCG_VERSION}/${weekday}
    localspace=/build/workspace/nightlies-tarfiles/${LCG_VERSION}/${weekday}
else
    basespace=/eos/project/l/lcg/www/lcgpackages/tarFiles/releases
    localspace=/build/workspace/releases-tarfiles
fi

eos ls -l $basespace/$txtfile 2>/dev/null && xrdfs ${EOS_MGM_URL} rm $basespace/$txtfile
cd $localspace

if [ "${BUILDMODE}" == "nightly" ]; then 
    eos ls -l ${basespace}/${isDone} 2>/dev/null && xrdfs ${EOS_MGM_URL} rm ${basespace}/${isDone}
    eos ls -l ${basespace}/${isDoneUnstable} 2>/dev/null && xrdfs ${EOS_MGM_URL} rm ${basespace}/${isDoneUnstable}
    while read oldtar
    do
        xrdfs ${EOS_MGM_URL} rm ${basespace}/${oldtar}
    done < <(eos ls "${basespace}/${tarfiles}")

    [ -e ${isDone} ] && xrdcp -f --posc  ${isDone} ${EOS_MGM_URL}/${basespace}/${isDone}?eos.atomic=1
    [ -e ${isDoneUnstable} ] && xrdcp -f --posc ${isDoneUnstable} ${EOS_MGM_URL}/${basespace}/${isDoneUnstable}?eos.atomic=1
fi

xrdcp -f ${txtfile} ${EOS_MGM_URL}/${basespace}/${txtfile}

find . -type f | sed -e 's/.*\.//' | sort | uniq -c | sort -n | grep -Ei '(tgz)$'
if [ $? != 0 ]; then
    echo "there are no .tgz files in this directory, we just go out"
    exit 0
else
    for files in `ls *${PLATFORM}*.tgz; ls -1 *${SHORTPLATFORM}*.tgz`
    do
        if [ "${BUILDMODE}" == "nightly" ]; then
	        xrdcp -f --posc  $files ${EOS_MGM_URL}/$basespace/${files}?eos.atomic=1
            rm $files
        else
            xrdcp --posc $files ${EOS_MGM_URL}/$basespace/${files}?eos.atomic=1
	        rm $files
        fi
    done
fi
