#!/bin/bash                                                                                                                                                                                                                                 

if [ $# -ge 4 ]; then
  RELEASE=$1; shift
  PLATFORM=$1; shift
  RPM_REVISION=$1; shift
  PACKAGE=$1; shift
else
  echo "$0: expecting 4 arguments: [release] [platform] [rpm_revision] [package]"
  return
fi

export RELEASE
export PLATFORM
export RPM_PLATFORM=`echo $PLATFORM | sed s/-/_/g`
export RPM_REVISION
export PACKAGE
export WORKDIR=$WORKSPACE

rpm_externals=LCG_$RELEASE\_$RPM_PLATFORM-1.0.0-$RPM_REVISION
echo $rpm_externals
rpm_generators=LCG_generators_$RELEASE\_$RPM_PLATFORM-1.0.0-$RPM_REVISION

# Starting the cvmfs commands to ensure a transaction
sudo -i -u cvsft <<EOF
cvmfs_server transaction sft.cern.ch

if [ "\$?" == "1" ]; then
  echo "Transaction is already open. Exiting ..."
  exit 1
fi


echo "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
whoami
echo "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"

export MYSITEROOT=/cvmfs/sft.cern.ch/lcg/releases/

echo "Clean temporary rpmdb files:"
rm -fv /cvmfs/sft.cern.ch/lcg/releases/var/lib/rpm/__db.00*

abort=0
if [[ $PACKAGE == *all* ]]; then
    /afs/cern.ch/sw/lcg/app/spi/tools/LCGRPM/install/lcg_install.sh --rpmupdate install $rpm_externals || abort=1
    /afs/cern.ch/sw/lcg/app/spi/tools/LCGRPM/install/lcg_install.sh --rpmupdate install $rpm_generators || abort=1
else
    /afs/cern.ch/sw/lcg/app/spi/tools/LCGRPM/install/lcg_install.sh --rpmupdate install $rpm_generators || abort=1
fi

if [ -s $WORKDIR/lcgcmake/cmake/scripts/create_lcg_view.py ]; then
  python $WORKDIR/lcgcmake/cmake/scripts/create_lcg_view.py -r ${RELEASE} -l /cvmfs/sft.cern.ch/lcg/releases -p ${PLATFORM} -B /cvmfs/sft.cern.ch/lcg/views/LCG_${RELEASE}/${PLATFORM} || abort=1
else
  echo "create_lcg_view.py not found"
fi


# sed -i '\$a /lcg/views/LCG_${RELEASE}/*/*' /cvmfs/sft.cern.ch/.cvmfsdirtab

cd $HOME
test "\$abort" == "1" && cvmfs_server abort sft.cern.ch || echo Changes will be published
cvmfs_server publish  sft.cern.ch
EOF

