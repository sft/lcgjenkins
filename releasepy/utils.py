# -*- coding: utf-8 -*-

"""
releasepy.utils
~~~~~~~~~~~~~~
This module provides utility functions that are used within releasepy
that are also useful for external consumption.
"""

import os
import requests
import paths
import subprocess
import shutil
import filecmp

def checkURL(url):
  if "file://" in url:
    return True if os.system('stat {0} 1>/dev/null 2>/dev/null'.format(url.replace('file://', ''))) == 0 else False
  ret = requests.head(url)
  return ret.status_code == 200

def getCompilerPath(version, platform, endsystem):
    path = paths.gcc(endsystem)

    if os.path.exists(os.path.join(path, version)):
        path = os.path.join(path, version)
    elif os.path.exists(os.path.join(os.path.join(path, '.'.join(version.split('.')[:-1])))):
        path = os.path.join(os.path.join(path, '.'.join(version.split('.')[:-1])))
    if os.path.exists(os.path.join(path, platform)):
        return os.path.join(os.path.join(path, platform))
    else:
        raise RuntimeError("Cannot find compiler in {0}".format(os.path.join(path, platform)))

def splitall(path):
    allparts = []
    while 1:
        parts = os.path.split(path)
        if parts[0] == path:  # sentinel for absolute paths
            allparts.insert(0, parts[0])
            break
        elif parts[1] == path:  # sentinel for relative paths
            allparts.insert(0, parts[1])
            break
        else:
            path = parts[0]
            allparts.insert(0, parts[1])
    return allparts

def searchLinks(toPath, fromPaths):
    expandedPaths = fromPaths
    existingLinks = []
    for p in expandedPaths:
        for path in os.listdir(p):
            realpath = os.path.realpath(os.path.join(p, path))
            if realpath == toPath:
                # print "Link exists from:",os.path.join(p,path)
                existingLinks.append(os.path.join(p, path))
    print "There are {0} links pointing to: {1}\n{2}".format(len(existingLinks), toPath, "\n\t".join(existingLinks))
    return existingLinks

def getPlatformFromFilename(descfilename):
    return '_'.join(descfilename.replace('.txt', '').split('_')[2:])

def getVersionFromFilename(descfilename):
    return descfilename.replace('.txt', '').split('_')[2:]

def postinstall(postfile, installdir):
    p = subprocess.Popen(['env', 'INSTALLDIR={0}'.format(installdir), 'bash', postfile], stdout=subprocess.PIPE,
        stderr=subprocess.PIPE)
    stdout, stderr = p.communicate()
    if p.returncode == 0:
        print "  OK."
        return True
    else:
        print "  ERROR:"
        print stderr.strip()
        raise RuntimeError("Error in post-install step")


def getFolderSize(folder):
    # Difference is slightly higher using this method instead of du from system
    # total_size = os.path.getsize(folder)
    # for item in os.listdir(folder):
    #     itempath = os.path.join(folder, item)
    #     if os.path.isfile(itempath):
    #         total_size += os.path.getsize(itempath)
    #     elif os.path.isdir(itempath):
    #         total_size += getFolderSize(itempath)
    p = subprocess.Popen(["du", '--apparent-size',folder, "--max-depth=0"], stdout=subprocess.PIPE,
        stderr=subprocess.PIPE)
    stdout, stderr = p.communicate()
    total_size = stdout.split('\t')[0]
    return int(total_size)

def checkDirSize(dirToCheck, dirToCompare, threshold):
    print "Checking size..."
    sizeInstalled = getFolderSize(dirToCheck)
    sizeTemporal  = getFolderSize(dirToCompare)
    if abs(sizeInstalled - sizeTemporal) > threshold:
        print "[x] Size does not match: ",dirToCheck
        print " - Expected size: ",sizeTemporal
        print " - Real size: ",sizeInstalled
        return False
    else:
        print "[v] Size is correct"
        return True

def checkDirContent(dirToCheck, dirToCompare, debug):
    # Comparing trees
    print "Checking content..."
    dc = filecmp.dircmp(dirToCheck, dirToCompare)
    if not dc.left_only and not dc.right_only:
        print "[v] Package successfully installed: ",dirToCheck
        return True
    else:
        print "[x] Package wrong installed: ", dirToCheck
        if debug:
            print 'Diff:'
            print ' - Installed:', dc.left_only
            print ' - EOS tar:', dc.right_only
        return True

def unTAR(package, sourcepath, prefix, opts='-xpz'):
      opts += " --show-transformed-names --transform 's,/{0},/{0}-{1},g'".format(package.getVersion(), package.getHash())  # verbose (to keep list of untared files)
      filename = os.path.join(sourcepath, package.getPackageFilename())
      p = subprocess.Popen('curl -s {0} | tar {1} -C {2} -f -'.format(filename, opts, prefix),
          stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
      stdout, stderr = p.communicate()
      print stdout
      if p.returncode == 0:
        print "File:", filename, "Extracted:", len(stdout.strip().split('\n')), 'files'
        return True
      else:
        print "  ERROR: cannot properly run the following command:"
        print '  curl -s', filename, ' | tar', opts, '-C', prefix, '-f', '-'
        print stderr
        if len(stdout.strip()) != 0:
          tarprefix = sorted(stdout.strip().split('\n'))[0]
          tarprefix = os.path.join(prefix, tarprefix)
          print "  Try to revert changes: rm -rf {0}".format(tarprefix)
          try:
            if tarprefix != "":
                shutil.rmtree(tarprefix)
            print "  FAILED. But installation directory should be clean."
          except:
            print "  ERROR: cannot remove " + tarprefix
          raise RuntimeError(stderr)
        else:
          raise RuntimeError("Error during extraction.")
