#!/bin/bash -xe
mkdir -p $WORKSPACE
cp -r /lcg* $WORKSPACE/
ls -la $WORKSPACE/lcg-csh-test

cd $WORKSPACE
bash -x $WORKSPACE/lcg-csh-test/runtest-docker.sh "$1" "$2"
