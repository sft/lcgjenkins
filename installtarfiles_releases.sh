#!/bin/bash -x                                                                                                                                                                                                                           
if [ $# -ge 2 ]; then
  PLATFORM=$1; shift

else
  echo "$0: expecting 2 argument: [platform] [endsystem]"
  return
fi

export PLATFORM
export WORKDIR=$WORKSPACE
export ENDSYSTEM=$LABEL

if [[ $ENDSYSTEM == *cvmfs* ]]; then
base="/cvmfs/sft.cern.ch/lcg/releases"
else
basewrite="/afs/.cern.ch/sw/lcg/releases"
baseread="/afs/cern.ch/sw/lcg/releases"
kinit sftnight@CERN.CH -5 -V -k -t /ec/conf/sftnight.keytab
fi

array=(${PLATFORM//-/ })
comp=`echo ${array[2]}`
if [[ $ENDSYSTEM == *cvmfs* ]]; then
# Starting the cvmfs commands to ensure a transaction
sudo -i -u cvsft <<EOF
cvmfs_server transaction sft.cern.ch

if [ "\$?" == "1" ]; then
  echo "Transaction is already open. Exiting ..."
  exit 1
fi

abort=0

$WORKSPACE/lcgjenkins/lcginstall.py -u  https://lcgpackages.web.cern.ch/tarFiles/releases -r ${LCG_VERSION} -d LCG_${LCG_VERSION}_$PLATFORM.txt -p $base
cd $base/${LCG_VERSION}
$WORKSPACE/lcgjenkins/splitglobalsummary.py ${LCG_VERSION} $PLATFORM $comp Release 
test "\$abort" == "2" && $WORKSPACE/lcgcmake/cmake/scripts/create_lcg_view.py -r ${LCG_VERSION} -l $base -p $PLATFORM -d -B $base/$PLATFORM 

cd $HOME
cvmfs_server publish  sft.cern.ch
EOF
else
$WORKSPACE/lcgjenkins/lcginstall.py -u  https://lcgpackages.web.cern.ch/tarFiles/releases -r ${LCG_VERSION} -d LCG_${LCG_VERSION}_$PLATFORM.txt -p $basewrite
cd $base/${LCG_VERSION}                                                                                                                                                                                          
$WORKSPACE/lcgjenkins/splitglobalsummary.py ${LCG_VERSION} $PLATFORM $comp Release        
afs_admin vos_release p.sw.lcg.releases
afs_admin vos_release p.sw.lcg.relmcgen
$WORKSPACE/lcgcmake/cmake/scripts/create_lcg_view.py -r ${LCG_VERSION} -l $baseread -p $PLATFORM -d -B $basewrite/$PLATFORM
afs_admin vos_release p.sw.lcg
fi
