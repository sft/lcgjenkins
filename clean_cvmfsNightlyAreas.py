#!/usr/bin/python3

from __future__ import print_function

import sys, glob, os
import shutil
import subprocess
import Tools as tools

# Removal of nightly builds from CVMFS

##########################
if __name__ == "__main__":
    from optparse import OptionParser
    parser = OptionParser()

    (options, args) = parser.parse_args()
    if len(args)!=2:
        print("Please provide a slot name and day of the week to clean!")
        sys.exit(-2)
    slot = args[0]
    day_option = args[1]

    if day_option=="tomorrow":
        day = tools.tomorrow()
    else:
        day = day_option

    BASE="/cvmfs/sft-nightlies.cern.ch/lcg/nightlies"

    print("Cleaning nightly builds of slot '%s' for day %s" % (slot,day))

    rootdir = tools.pathJoin(BASE,slot,day)

    for fileType in ['isDone', 'LCG_externals', 'LCG_generators', 'LCG_contrib']:
        print('Removing the %s files' % fileType)
        command = 'rm %s/%s*' % (rootdir, fileType)
        tools.executeAndCheckCommand(command,
                                     "%s files removed" % fileType,
                                     "%s files not removed" % fileType,
                                     exitOnError=False)

    dirs = glob.glob(os.path.join(rootdir, '*/*'))
    files = glob.glob(os.path.join(rootdir, '*/.*'))

    for f in files:
        os.remove(f)

    for d in dirs:
        try:
            if os.path.islink(d):
                print('Unlinking directory %s' % (d))
                os.unlink(d)
            else:
                print('Removing directory %s' % (d))
                shutil.rmtree(d)
        except Exception as e:
            print("Failed removing directory", repr(e))

    command = 'rmdir %s/*' %(rootdir)
    tools.executeAndCheckCommand(command,"empty package dirs removed","empty package dirs not removed", exitOnError=False)
