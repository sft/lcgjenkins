#!/usr/bin/env bash

# link_contrib.sh $pkg_dir $dest_dir $platform
# link_contrib creates the same layout as currently seen in /cvmfs/sft.cern.ch/lcg/contrib

function print_help() {
    echo "link_contrib.sh [-s|--short-platform] [--platform PLATFORM] pkg_dir dest_dir platform"
    echo pkd_dir    - Points to location with buildinfo
    echo dest_dir   - Points to directory where the link will be created 
    echo              The script will create structure: pkgname/pkgversion/platform
    echo              The platform will be a relative link to the pkg_dir
    echo --platfrom - Full platform name of the package if not provided in 
    echo --short-platform - Cuts the platform name to \$arch-\$os 
    echo --name     - Package name
    echo --verbose
    echo
    exit 0
}

POSITIONAL=()
while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    -s|--short-platform)
    SHORTPLATFORM=YES
    shift 
    ;;
    --with-hash)
    echo "Option not implemented"
    exit 1
    ;;
    -n|--name)
    PKG_NAME=$2
    shift
    shift
    ;;
    -p|--platform)
    PLATFORM=$2
    shift
    shift
    ;;
    -v|--verbose)
    VERBOSE=YES
    shift
    ;;
    --help)
    print_help
    ;;
    *)    # unknown option
    POSITIONAL+=("$1") # save it in an array for later
    shift # past argument
    ;;
esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters

pkg_dir=$1
dest_dir=$2

if [ ! $dest_dir ]; then
    echo Error: to few arguments
    echo
    print_help
fi

if [ ! $PKG_NAME ]; then
    PKG_NAME=${pkg_dir/\/*/} # Assume that first part of path is the name: name/version/platform
fi

function relative_path () {
    # https://stackoverflow.com/a/12498485
    # both $1 and $2 are absolute paths beginning with /
    # returns relative path to $2/$target from $1/$sources
    sources=$1
    target=$2
    
    common_part=$sources # for now
    result="" # for now
    
    while [[ "${target#$common_part}" == "${target}" ]]; do
        # no match, means that candidate common part is not correct
        # go up one level (reduce common part)
        common_part="$(dirname $common_part)"
        # and record that we went back, with correct / handling
        if [[ -z $result ]]; then
            result=".."
        else
            result="../$result"
        fi
    done
    
    if [[ $common_part == "/" ]]; then
        # special case for root (no common path)
        result="$result/"
    fi
    
    # since we now have identified the common part,
    # compute the non-common part
    forward_part="${target#$common_part}"
    
    # and now stick all parts together
    if [[ -n $result ]] && [[ -n $forward_part ]]; then
        result="$result$forward_part"
    elif [[ -n $forward_part ]]; then
        # extra slash removal
        result="${forward_part:1}"
    fi
    
    echo $result
    
}

function relln () {
    [ $VERBOSE ] && echo SRC: $1 DEST: $2
    # Dereference symlink if exists
    dest=${2%/}
    if [ -e $dest ]; then
        [ $VERBOSE ] && echo rm $dest
        rm $dest
    else
        [ $VERBOSE ] && echo dont delete $dest
    fi
    # Create link
    export abs_src=$(readlink -e $1)
    [ ! "$abs_src" ] && echo Directory $1 does not exist !! && return
    export abs_dest=$(readlink -m $(dirname $dest))
    [ $VERBOSE ] && echo src: $abs_src dest: $abs_dest
    
    rel_src=$(relative_path $abs_dest $abs_src)
    dest_name=$(basename $2)

    if [ ! -e $abs_dest ]; then
        [ $VERBOSE ] && echo create directory
        mkdir -p $abs_dest
    fi
    pushd $abs_dest
    [ $VERBOSE ] && echo "$rel_src -> $dest_name"
    ln -s $rel_src $dest_name
    popd
}

function getinfo () {
    # Read information from buildinfo file provided inside the released package
    cat $pkg_dir/.buildinfo_*.txt | tr ',' '\n' | grep " $1: " | cut -f3 -d' '
}

if [ ! $PLATFORM ]; then
    PLATFORM=$(getinfo PLATFORM)
fi
if [ ! $PLATFORM ]; then
    echo Platform not specified and cannot be deduce
    echo Exit
    exit 1
fi

dest_platform=$PLATFORM
if [ $SHORTPLATFORM ]; then
    dest_platform=$(echo $PLATFORM | cut -f1-2 -d'-')
fi

# GCC
if [[ $PKG_NAME == *gcc* ]]; then
    version=$(getinfo VERSION) 
    major=$(echo $version | cut -f1 -d'.')
    minor=$(echo $version | cut -f2 -d'.')
    patch=$(echo $version | cut -f3 -d'.')

    #relln $pkg_dir $dest_dir/gcc/${version}/$dest_platform # 6.2.0 should be without binutils dependency
    relln $pkg_dir $dest_dir/gcc/${major}.${minor}.${patch}binutils/$dest_platform


    if [ $major -ge 7 ]; then
        relln $pkg_dir $dest_dir/gcc/${major}.${minor}.${patch}binutils/$dest_platform-gcc${major}-opt
        
        relln $pkg_dir $dest_dir/gcc/${version}/$dest_platform
        relln $pkg_dir $dest_dir/gcc/${version}/$dest_platform-gcc${major}-opt
        
        # For gcc 7 and higher the minor version is dropped as it is ABI compatible among the same major version
        relln $pkg_dir $dest_dir/gcc/${major}/$dest_platform
        relln $pkg_dir $dest_dir/gcc/${major}binutils/$dest_platform

        relln $pkg_dir $dest_dir/gcc/${major}/$dest_platform-gcc${major}-opt
        relln $pkg_dir $dest_dir/gcc/${major}binutils/$dest_platform-gcc${major}-opt
    else
        relln $pkg_dir $dest_dir/gcc/${major}.${minor}.${patch}binutils/$dest_platform-gcc${major}${minor}-opt
        
        #relln $pkg_dir $dest_dir/gcc/${major}.${minor}/$dest_platform
        relln $pkg_dir $dest_dir/gcc/${major}.${minor}binutils/$dest_platform

        relln $pkg_dir $dest_dir/gcc/${major}.${minor}binutils/$dest_platform-gcc${major}${minor}-opt
    fi

# CLANG
elif [[ $PKG_NAME == *clang* ]]; then
    version=$(getinfo VERSION)
    major=$(echo $version | cut -f1 -d'.')
    minor=$(echo $version | cut -f2 -d'.')

    relln $pkg_dir $dest_dir/llvm/${version}/$dest_platform
    relln $pkg_dir $dest_dir/llvm/${version}binutils/$dest_platform

    relln $pkg_dir $dest_dir/clang/${version}/$dest_platform
    relln $pkg_dir $dest_dir/clang/${version}binutils/$dest_platform
else
    version=$(getinfo VERSION)
    name=$(getinfo NAME)

    relln $pkg_dir $dest_dir/${name}/${version}/$dest_platform
fi
