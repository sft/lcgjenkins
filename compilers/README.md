This directory contains all task specific scripts needed to compile, prepare and install the compilers.
All the task in jenkins follow the convention *lcg\_contrib\_\* * with the main task *lcg\_contrib\_release*.
