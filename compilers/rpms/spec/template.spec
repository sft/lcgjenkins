%define lcgprefix opt/lcg
{% if SHORT_PLATFORM %}
%define platform {{ PLATFORM_SHORT }}
%define platform_u {{ PLATFORM_SHORT_U }}
{% else %}
%define platform {{ PLATFORM }}
%define platform_u {{ PLATFORM_U }}
{% endif %}

Name:       {{NAME}}_{{VERSION}}_%platform_u
Version:    2.0.0
Release:    {{REVISION}}
Summary:    {{NAME}} {{VERSION}} {{PLATFORM}}
Vendor:     EP-SFT

License:    GPL
Group:      LCG
Source0:    {{SOURCE}}
Prefix:     /%lcgprefix
AutoReqProv: no
BuildArch:  noarch
Distribution: whatever

Provides: /bin/sh
Provides: {{NAME}}_{{VERSION}}_%platform_u

{% for req in DEPENDSDICT %}Requires:   {{ req["NAME"] }}_{{ req["VERSION"] }}_%platform_u{% endfor %}

%description
{{NAME}}


%install
mkdir -p %{?buildroot}/%lcgprefix 
cd %{?buildroot}/%lcgprefix
tar xf %{_sourcedir}/{{NAME}}-{{VERSION}}_{{HASH}}-{{PLATFORM}}.tgz
{% if SHORT_PLATFORM %}
mv {{NAME}}/{{VERSION}}/{{PLATFORM}} {{NAME}}/{{VERSION}}/{{PLATFORM_SHORT}}
{% endif %}
{% if SETUP %}
create_setup.py --platform %platform {{NAME}}/{{VERSION}}/%platform
{% endif %}


%post
$RPM_INSTALL_PREFIX/{{NAME}}/{{VERSION}}/%platform/.post-install.sh "/%lcgprefix"

%files
/%lcgprefix

